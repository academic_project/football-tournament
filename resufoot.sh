#! /bin/bash

#2 arguments: $1 est le nbre de buts marquès par l'équipe 1, $2 est le nbre de buts marquès par l'équipe 2
#affiche:"(le nombre de points reçus pour l’équipe 1) (la différence entre le nombre de buts marqués par l’équipe 1 et le nombre de buts qu’elle a encaissé) (le nombre de buts marqués par l’équipe 1)"
function AffiScore {
	if [[ $1 -gt $2 ]]; then
		a=3
	elif [[ $2 -gt $1 ]]; then
		a=0
	else
		a=1
	fi
	
	echo $a $(($1-$2)) $1
}

#2 arguments: $1 est le le numéro n du match (entre 1 et 64), $2 est le le nom de l’équipe (par exemple « France »)
#affiche le score de l'équipe $2
function ResultatMatch {
	f="matchs/match$1.txt"
	#on vérifie si le fichier f existe
	if [[ -e $f ]]; then
		#on vérifie si le fichier f est en droit de lecture
		if [[ -r $f ]]; then
			e1=$(sed -n 2'p' $f) #on récupére le nom de l'équipe 1
			e2=$(sed -n 3'p' $f) #on récupére le nom de l'équipe 2
			#on regarde si $2 sorrespond à l'équipe 1 ou 2
			case $2 in
			"$e1")
				m=$(sed -n 4'p' $f) #la 4éme ligne du fichier f correspond au score de l'équipe 1 
				n=$(sed -n 5'p' $f) #la 5éme ligne du fichier f correspond au score de l'équipe 2
				;;
			"$e2")
				m=$(sed -n 5'p' $f)	
				n=$(sed -n 4'p' $f)
				;;
			*)
				echo "Mauvais numéro de match: Le match n°$1 n'implique pas l'équipe $2 dont vous voulez afficher le score!" 1>&2
				exit 64
				;;
			esac
			#on affiche le score de l'équipe m
			echo $(AffiScore $m $n)
		else
			echo "Fichier ./$f impossible à lire !" 1>&2
			exit 65
		fi
	else
		echo "Le fichier ./$f n'existe pas! Le numéro n du match doit être compris entre 1 et 64)" 1>&2
		exit 66
	fi
}

#1 argument: $1 est le nom de l'équipe (par exemple « France »)
function ResultatsEquipe {
	#parmi les matchs du premier tour (numérotés de 1 à 48)
	for (( i = 1; i < 49; i++ )); do
		#on veut afficher que les scores et pas les erreurs
		#et on supprime les lignes vides
		echo $(ResultatMatch $i "$1" 2>/dev/null) | awk 'NF'						
	done
}

#1 argument: $1 est le nom de l'équipe (par exemple « France »)
function AssemblerResultats {
	points=0
	diff=0
	buts=0
	#tant que la fonction a des lignes en entrée standard à lire
	while read p d b 
	do
		let "points=$points + $p"
		let "diff=$diff + $d"
		let "buts=$buts + $b"
	done
	echo $((500+$points))$((500+$diff))$((500+$buts)) \"$1\" $points $diff $buts
} 

#1 argument: $1 est le nom d'un groupe (par exemple « A »)
function ClasserGroupe {
	f="groupes/groupe$1.txt"
	if [[ ! -r $f ]]; then
		echo "Le fichier ./$f n'est pas lisible !" 1>&2
		exit 67
	fi
	while read team; do
		ResultatsEquipe "$team" | AssemblerResultats "$team" 
	done < "$f" > temp.txt
	#le tri va se faire par rapport à l'entier magique (par exemple "509505506")
	#l'option -n tri selon un ordre numérique (ordre croissant des nombres)
	#or on veut la meilleure équipe en premier donc on rajoute l'option -r
	sort -n -r < "temp.txt"
}

function usage {
	echo "Utilisation : $0 -e nom_equipe || $0 -[gh] nom_groupe" 1>&2
	exit 2 
}

#Programme prncipale du script
if [[ ! $# -eq 2 ]]; then
	usage
	exit 1
fi


case $1 in
-e)
	ResultatsEquipe $2
	;;	
-g)
	ClasserGroupe $2
	;;
-h)
	ClasserGroupe $2 | cut -d' ' -f 2- #on supprime le premier mot de chaque ligne
	;;
*)
	usage
	exit 1
	;;
esac

